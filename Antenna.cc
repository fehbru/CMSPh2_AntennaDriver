#include "Antenna.h"

Antenna::Antenna() {}

Antenna::~Antenna()
{
    if (fUsbHandle) close();
}

int Antenna::initializeAntenna(int trigsource)
{
    if ( ( fUsbHandle = setup_libusb_access() ) == NULL )
    {
        std::cout << "Abandon the ship! Failed to connect with antenna setup, check if it is plugged in the USB port." << std::endl;
        exit ( -1 ); // ok this is maybe a slight overreaction, but there is no point to continue testing if antenna test board is not connected
    }

    usb_claim_interface ( fUsbHandle, 0 ); // claim the interface for antenna connection, so kernel cannot do it when we need to use the device
    usb_reset ( fUsbHandle ); // chip needs to wake up from whatever state it was in before its interface was reclaimed
    //here: change mode of every pin!

    //set GPIO pins to push-pull mode, with idle value (high)
    char buf_out[3] = {0, 2, 1};
    char buf_out2[3]= {5, 2, 0};
    for (int cs_line = 0; cs_line < 10; cs_line++)
    {
        buf_out[0] = cs_line;
  //      int result = usb_control_msg ( fUsbHandle, 0x40, 0x23, 0, 0, ( char* ) buf_out, sizeof ( buf_out ), fUsbTimeout );
        if(trigsource==7 && cs_line ==5){
          int result = usb_control_msg ( fUsbHandle, 0x40, 0x23, 0, 0, ( char* ) buf_out2, sizeof ( buf_out ), fUsbTimeout );
          std::cout<<"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"<<std::endl; 
        }
        else{
          int result = usb_control_msg ( fUsbHandle, 0x40, 0x23, 0, 0, ( char* ) buf_out, sizeof ( buf_out ), fUsbTimeout );

        }

    }

    return 0;
}

void Antenna::close()
{
    /*We release interface so any other software or kernel driver can claim it */
    usb_release_interface ( fUsbHandle, 0 );
    /*we close the usb connection with the cp2130 chip*/
    usb_close ( fUsbHandle );
}

usb_dev_handle* Antenna::find_antenna_usb_handle()
{
    struct usb_bus* bus;
    struct usb_device* dev;

    for ( bus = usb_busses; bus; bus = bus->next )
    {
        for ( dev = bus->devices; dev; dev = dev->next )
        {
            if ( dev->descriptor.idVendor == VENDOR_ID &&
                    dev->descriptor.idProduct == PRODUCT_ID )
            {
                usb_dev_handle* handle;

                //std::cout << "antenna_usb_handle with Vendor Id: " << VENDOR_ID << " and Product Id: " << PRODUCT_ID << " found " << std::endl;
                if ( ! ( handle = usb_open ( dev ) ) )
                {
                    std::cout << "Could not open USB device" << std::endl;

                    return NULL;
                }

                return handle;
            }
        }
    }

    return NULL;
}

usb_dev_handle* Antenna::setup_libusb_access()
{
    usb_dev_handle* antenna_usb_handle;
    usb_set_debug ( 0 ); // 255 == on , 0 == off?
    usb_init();
    usb_find_busses();
    usb_find_devices();

    if ( ! ( antenna_usb_handle = find_antenna_usb_handle() ) )
    {
        std::cout << "Couldn't find the USB device, Exiting" << std::endl;
        std::cin.ignore();
        return NULL;
    }

    if ( usb_set_configuration ( antenna_usb_handle, 1 ) < 0 )
    {
        std::cout << "Could not set configuration 1 : " << std::endl;

        return NULL;
    }

    if ( usb_claim_interface ( antenna_usb_handle, INTERF ) < 0 )
    {
        std::cout << "Could not claim interface: " << std::endl;

        return NULL;
    }

    return antenna_usb_handle;
}

void Antenna::ConfigureClockGenerator ( uint8_t pSlaveChipSelectId, uint8_t pFrequencyDivider, bool pEnableClock)
{
    ConfigureSpiSlave (pSlaveChipSelectId);
    int result; // variable for gathering results from USB related transfers, useful only for debugging
    char bulk_buffer_out[10] = {0, 0, 1, 0, 2, 0, 0, 0, 0, 0}; //SPI communication values
    bulk_buffer_out[9] = ( char ) ( (pEnableClock) ? 0x00 : 0x03);
    bulk_buffer_out[8] = ( char ) (  ( pFrequencyDivider << 4 ) & 0xFF ) ;
    result = usb_bulk_write ( fUsbHandle, fUsbEndpointBulkOut, ( char* ) bulk_buffer_out, sizeof ( bulk_buffer_out ), fUsbTimeout );
    //printf("Results of clock frequency generator byte 0: %d\n", result);
    sleep ( 0.1 );
}

float Antenna::ConvertADCtoTemperature (float pADCValue)
{
    const static int fTempLookUpTableSize = 34;
    float fTempLookUpTable[fTempLookUpTableSize] = {848, 804, 755, 701, 643, 584, 525, // 848 corresponds to -40 deg C, steps every 5 degree
                                                    467, 413, 362, 315, 273, 236, 203, 175, 150, 129, 111, 95,
                                                    82, 71, 61, 53, 46, 40, 35, 31, 27, 24, 21, 18, 16, 14, 13
                                                   }; // 13 corresponds to 125 deg C

    if (pADCValue >= fTempLookUpTable[0]) return -40.0;
    else if (pADCValue <= fTempLookUpTable[fTempLookUpTableSize - 1]) return 125.0;
    else
    {
        for (int i = 1; i < fTempLookUpTableSize; i++)
        {
            if (pADCValue > fTempLookUpTable[i]) return (5 * i - 40 - 5 * (pADCValue - fTempLookUpTable[i]) / (fTempLookUpTable[i - 1] - fTempLookUpTable[i]) );
        }
    }
}

float Antenna::GetHybridTemperature (uint8_t pSlaveChipSelectId)
{
    ConfigureSlaveADC (pSlaveChipSelectId);
    int result; // variable for gathering results from USB related transfers, useful only for debugging
    char cnfg_0_byte = 0b11100011; //AIN2
    char bulk_buffer_out[12] = {0, 0, 2, 0, 4, 0, 0, 0, cnfg_0_byte, 0xFF, 0xFF, 0xFF};
    result = usb_bulk_write ( fUsbHandle, fUsbEndpointBulkOut, ( char* ) bulk_buffer_out, sizeof ( bulk_buffer_out ), fUsbTimeout );

    char adc_read_buffer[4] = {0};
    result = usb_bulk_read (fUsbHandle, fUsbEndpointBulkIn, ( char* ) adc_read_buffer, sizeof (adc_read_buffer), fUsbTimeout);


    if (result != 4 )
    {
        printf ("USB Transaction ERROR!\n");
        return -1000.0;
    }
    else if (adc_read_buffer[1] != cnfg_0_byte)
    {
        printf ("SPI Transaction ERROR!\n");
        return -1000.0;
    }
    else
    {
        uint16_t ADC_value = ( ( (adc_read_buffer[2] & 0x0F) << 6) + ( (adc_read_buffer[3] >> 1) & 0x3F) );
        //printf("Hybrid Temperature ADC = %d\n", ADC_value);
        float hybrid_temperature = ConvertADCtoTemperature ( (float) ADC_value);
        //printf("Hybrid Temperature = %.2f deg C\n", hybrid_temperature);
        return hybrid_temperature;
    }

}

float Antenna::GetHybridCurrent (uint8_t pSlaveChipSelectId)
{
    ConfigureSlaveADC (pSlaveChipSelectId);
    float V_ref = 1.25;
    int result; // variable for gathering results from USB related transfers, useful only for debugging
    //char bulk_buffer_out[17] = {0, 0, 2, 0, 9, 0, 0, 0, 0xE3, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }; //SPI communication value, send 1 byte to SPI
    char cnfg_0_byte = 0b11010011; //AIN1
    char bulk_buffer_out[12] = {0, 0, 2, 0, 4, 0, 0, 0, cnfg_0_byte, 0xFF, 0xFF, 0xFF};
    //bulk_buffer_out[8] = ( char )( 0xE1 );
    result = usb_bulk_write ( fUsbHandle, fUsbEndpointBulkOut, ( char* ) bulk_buffer_out, sizeof ( bulk_buffer_out ), fUsbTimeout );
    char adc_read_buffer[4] = {0};
    result = usb_bulk_read (fUsbHandle, fUsbEndpointBulkIn, ( char* ) adc_read_buffer, sizeof (adc_read_buffer), fUsbTimeout);
    //printf("Results of ADC byte 0 read: %d\n", result);

    if (result != 4 )
    {
        printf ("USB Transaction ERROR!\n");
        return -1000.0;
    }
    else if (adc_read_buffer[1] != cnfg_0_byte)
    {
        printf ("SPI Transaction ERROR!\n");
        return -1000.0;
    }
    else
    {
        uint16_t ADC_value = ( ( (adc_read_buffer[2] & 0x0F) << 6) + ( (adc_read_buffer[3] >> 1) & 0x3F) );
        //printf("adc value = %d\n", ADC_value);
        float ADC_voltage = ( (ADC_value * V_ref) * 1000) / 1023;
        //printf("Hybrid current = %.0f mA\n", ADC_voltage);
        return ADC_voltage;
    }

}

float Antenna::GetAmuxVoltage (uint8_t pSlaveChipSelectId)
{
    ConfigureSlaveADC (pSlaveChipSelectId);
    float V_ref = 1.25;
    int result; // variable for gathering results from USB related transfers, useful only for debugging
    //char bulk_buffer_out[17] = {0, 0, 2, 0, 9, 0, 0, 0, 0xE3, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }; //SPI communication value, send 1 byte to SPI
    char cnfg_0_byte = 0b11000011; //AIN0
    char bulk_buffer_out[12] = {0, 0, 2, 0, 4, 0, 0, 0, cnfg_0_byte, 0xFF, 0xFF, 0xFF};
    //bulk_buffer_out[8] = ( char )( 0xE1 );
    result = usb_bulk_write ( fUsbHandle, fUsbEndpointBulkOut, ( char* ) bulk_buffer_out, sizeof ( bulk_buffer_out ), fUsbTimeout );
    char adc_read_buffer[4] = {0};
    result = usb_bulk_read (fUsbHandle, fUsbEndpointBulkIn, ( char* ) adc_read_buffer, sizeof (adc_read_buffer), fUsbTimeout);
    //printf("Results of ADC byte 0 read: %d\n", result);

    /* USE THIS FOR DEBUGGING OF THE SPI MESSAGE
    printf("Return message: ");
        for (int i = 0; i< sizeof(adc_read_buffer); i++) printf("%d ", (uint8_t)adc_read_buffer[i]);
    printf("\n");
    */

    if (result != 4 )
    {
        printf ("USB Transaction ERROR!\n");
        return -1000.0;
    }
    else if (adc_read_buffer[1] != cnfg_0_byte)
    {
        printf ("SPI Transaction ERROR!\n");
        return -1000.0;
    }
    else
    {
        uint16_t ADC_value = ( ( (adc_read_buffer[2] & 0x0F) << 6) + ( (adc_read_buffer[3] >> 1) & 0x3F) );
        //printf("adc value = %d\n", ADC_value);
        float ADC_voltage = 1000 * (ADC_value * V_ref) / 1023;
        //printf("Amux Voltage = %.1f mV\n", ADC_voltage);
        return ADC_voltage;
    }

}

void Antenna::ConfigureADC ( uint8_t pSlaveChipSelectId)
{
    printf ("ADC SPI channel: %u\n", pSlaveChipSelectId);
    ConfigureSlaveADC (pSlaveChipSelectId); // ADC is on channel 4

    printf ("Hybrid Temperature = %.2f deg C\n", GetHybridTemperature() );
    printf ("Hybrid current = %.1f mA\n", GetHybridCurrent() );
    printf ("Amux Voltage = %.1f mV\n", GetAmuxVoltage() );

}

void Antenna::ConfigureDigitalPotentiometer ( uint8_t pSlaveChipSelectId, uint16_t pRdacValue )
{
    ConfigureSpiSlave (pSlaveChipSelectId);
    int result; // variable for gathering results from USB related transfers, useful only for debugging
    char bulk_buffer_out[11] = {0, 0, 1, 0, 3, 0, 0, 0, 0xB0, 0x01, 0xD8}; //SPI communication values 1 D8
    //bulk_buffer_out[10] = ( char )(pRdacValue & 0xFF);
    //bulk_buffer_out[9] = ( char )((pRdacValue >> 8) & 0xFF);
    //bulk_buffer_out[8] = ( char )(0xB0) ;
    result = usb_bulk_write ( fUsbHandle, fUsbEndpointBulkOut, ( char* ) bulk_buffer_out, sizeof ( bulk_buffer_out ), fUsbTimeout );
    //printf("Results of digital potentiometer 0: %d\n", result);
    sleep ( 0.1 );

}

void Antenna::ConfigureSpiSlave ( uint8_t pSlaveChipSelectId )
{
    int result; // variable for gathering results from USB related transfers, useful only for debugging
    char buf_in[4] = {0}; // buf_in[] is used just for reading back data from chip, via usb, I used it only for debugging purpose to check if correct CS line has been set for SPI transfers
    char control_msg_set_spi_word[2] = {0, 0b00001101}; //configuration values from CP2130 datasheet
    char buf_out[2] = {0, 2};  //configuration values from CP2130 datasheet
    char bulk_buffer_out[9] = {0, 0, 1, 0, 1, 0, 0, 0, 0}; //SPI communication values, bits of the last byte are the channels or the analog switch to be turned on
    buf_out[0] = pSlaveChipSelectId;
    control_msg_set_spi_word[0] = pSlaveChipSelectId;

    /*Set SPI transfer parameters.*/
    result = usb_control_msg ( fUsbHandle, 0x40, 0x31, 0, 0, ( char* ) control_msg_set_spi_word, sizeof ( control_msg_set_spi_word ), fUsbTimeout );

    /*Activate chip select line of corresponding slave for SPI communication.*/
    result = usb_control_msg ( fUsbHandle, 0x40, 0x25, 0, 0, ( char* ) buf_out, sizeof ( buf_out ), fUsbTimeout );

    /*Check 'buf_in' if correct number of chip select channel was stored in the cp2130 chip.*/
    result = usb_control_msg ( fUsbHandle, 0xC0, 0x24, 0, 0, ( char* ) buf_in, sizeof ( buf_in ), fUsbTimeout );

    /*Finally we can write through cp2130 to analog switch. We are writing an array of chars where the last byte is giving the position of channels to be turned on.
    They are identified by '1' position in binary representation of that byte value.
    result = usb_bulk_write( fUsbHandle, fUsbEndpointBulkOut, ( char* ) bulk_buffer_out, sizeof( bulk_buffer_out ), fUsbTimeout ); // turning off all channels of analog switch
    */
    sleep ( 0.1 );
}

void Antenna::ConfigureSlaveADC (uint8_t pSlaveChipSelectId )
{
    int result; // variable for gathering results from USB related transfers, useful only for debugging
    char buf_in[4] = {0}; // buf_in[] is used just for reading back data from chip, via usb, I used it only for debugging purpose to check if correct CS line has been set for SPI transfers
    char control_msg_set_spi_word[2] = {0, 0b00111101}; //configuration values from CP2130 datasheet
    char buf_out[2] = {0, 2};  //configuration values from CP2130 datasheet
    char bulk_buffer_out[9] = {0, 0, 1, 0, 1, 0, 0, 0, 0}; //SPI communication values, bits of the last byte are the channels or the analog switch to be turned on
    buf_out[0] = pSlaveChipSelectId;
    control_msg_set_spi_word[0] = pSlaveChipSelectId;

    char spi_delay[8] = {4, 0x0F,
                         0, 1, //inter-byte delay *10us big endian 2 bytes
                         0, 0, //post-assert delay *10us big endian 2 bytes
                         0, 1  //pre-deassert delay *10us big endian 2 bytes
                        };



    /*Set SPI transfer parameters.*/
    result = usb_control_msg ( fUsbHandle, 0x40, 0x31, 0, 0, ( char* ) control_msg_set_spi_word, sizeof ( control_msg_set_spi_word ), fUsbTimeout );

    result = usb_control_msg ( fUsbHandle, 0x40, 0x33, 0, 0, ( char* ) spi_delay, sizeof ( spi_delay ), fUsbTimeout );

    /*Activate chip select line of corresponding slave for SPI communication.*/
    result = usb_control_msg ( fUsbHandle, 0x40, 0x25, 0, 0, ( char* ) buf_out, sizeof ( buf_out ), fUsbTimeout );




    /*Check 'buf_in' if correct number of chip select channel was stored in the cp2130 chip.*/
    result = usb_control_msg ( fUsbHandle, 0xC0, 0x24, 0, 0, ( char* ) buf_in, sizeof ( buf_in ), fUsbTimeout );

    /*Finally we can write through cp2130 to analog switch. We are writing an array of chars where the last byte is giving the position of channels to be turned on.
    They are identified by '1' position in binary representation of that byte value.
    result = usb_bulk_write( fUsbHandle, fUsbEndpointBulkOut, ( char* ) bulk_buffer_out, sizeof( bulk_buffer_out ), fUsbTimeout ); // turning off all channels of analog switch
    */
    sleep ( 0.1 );
}

void Antenna::ConfigureAnalogueSwitch ( uint8_t pSlaveChipSelectId )
{
    int result; // variable for gathering results from USB related transfers, useful only for debugging
    char buf_in[4] = {0}; // buf_in[] is used just for reading back data from chip, via usb, I used it only for debugging purpose to check if correct CS line has been set for SPI transfers
    char control_msg_set_spi_word[2] = {0, 0x19}; //configuration values from CP2130 datasheet
    char buf_out[2] = {0, 2};  //configuration values from CP2130 datasheet
    char bulk_buffer_out[9] = {0, 0, 1, 0, 1, 0, 0, 0, 0}; //SPI communication values, bits of the last byte are the channels or the analog switch to be turned on
    buf_out[0] = pSlaveChipSelectId;
    control_msg_set_spi_word[0] = pSlaveChipSelectId;

    /*Set SPI transfer parameters.*/
    result = usb_control_msg ( fUsbHandle, 0x40, 0x31, 0, 0, ( char* ) control_msg_set_spi_word, sizeof ( control_msg_set_spi_word ), fUsbTimeout );

    /*Activate chip select line of corresponding slave for SPI communication.*/
    result = usb_control_msg ( fUsbHandle, 0x40, 0x25, 0, 0, ( char* ) buf_out, sizeof ( buf_out ), fUsbTimeout );

    /*Check 'buf_in' if correct number of chip select channel was stored in the cp2130 chip.*/
    result = usb_control_msg ( fUsbHandle, 0xC0, 0x24, 0, 0, ( char* ) buf_in, sizeof ( buf_in ), fUsbTimeout );

    /*Finally we can write through cp2130 to analog switch. We are writing an array of chars where the last byte is giving the position of channels to be turned on.
    They are identified by '1' position in binary representation of that byte value.*/
    result = usb_bulk_write ( fUsbHandle, fUsbEndpointBulkOut, ( char* ) bulk_buffer_out, sizeof ( bulk_buffer_out ), fUsbTimeout ); // turning off all channels of analog switch
    sleep ( 0.1 );
}

void Antenna::TurnOnAnalogSwitchChannel ( uint8_t pSwichChannelId )
{
    int result; // variable for gathering results from USB related transfers, useful only for debugging
    char bulk_buffer_out[9] = {0, 0, 1, 0, 1, 0, 0, 0, 0}; //SPI communication values, bits of the last byte are the channels or the analog switch to be turned on

    if ( pSwichChannelId == 9 )
    {
        bulk_buffer_out[8] = 0; // this is just to turn off all the channels of analog switch (if powered it holds last written configuration) at the end of the loop
    }
    else
        bulk_buffer_out[8] = ( char ) ( ( 1 << ( pSwichChannelId - 1 ) ) & 0xFF );

    //#ifdef SLC5_
    result = usb_bulk_write ( fUsbHandle, fUsbEndpointBulkOut, ( char* ) bulk_buffer_out, sizeof ( bulk_buffer_out ), fUsbTimeout );
    //#elif
    //  result = whatever u have to use in SLC6
    //#endif
    sleep ( 0.1 );

}
