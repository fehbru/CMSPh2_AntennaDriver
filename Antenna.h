#ifndef Antenna_h__
#define Antenna_h__

#include <usb.h>
#include <iostream>
#include <sstream>

#define VERSION "0.1.0"
#define VENDOR_ID 0x10C4
#define PRODUCT_ID 0x87A0
#define INTERF 0
#define ADC_SLAVE_ID 4

namespace patch {
    template < typename T > std::string to_string ( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

class Antenna
{
  public:
    Antenna();

    ~Antenna();

    int initializeAntenna(int trigsource);

    void close();

    /*!
    * \brief public method that configures SPI interface between CP2130 and slave analog switch
    */
    void ConfigureSpiSlave ( uint8_t pSlaveChipSelectId );
    void ConfigureAnalogueSwitch ( uint8_t pSlaveChipSelectId );
    void ConfigureSlaveADC ( uint8_t pSlaveChipSelectId );

    /*!
    * \brief public method that switches on given channel of last analog switch for which SPI interface was configured
    */
    void TurnOnAnalogSwitchChannel ( uint8_t pSwichChannelId );

    /*!
    * \brief public method to configure the clock frequency generator
    */
    void ConfigureClockGenerator ( uint8_t pSlaveChipSelectId, uint8_t pFrequencyDivider, bool pEnableClock = true);

    void ConfigureDigitalPotentiometer ( uint8_t pSlaveChipSelectId, uint16_t pRdacValue );

    void ConfigureADC ( uint8_t pSlaveChipSelectId = ADC_SLAVE_ID);

    /*!
    * \brief public method that returns the AMUX voltage expressed in mV (resolution = 1.22 mV)
    */
    float GetAmuxVoltage (uint8_t pSlaveChipSelectId = ADC_SLAVE_ID);
    /*!
    * \brief public method that returns the hybrid current expressed in mA (resolution = 1.22 mA)
    */
    float GetHybridCurrent (uint8_t pSlaveChipSelectId = ADC_SLAVE_ID);
    /*!
    * \brief public method that returns the hybrid temperature measured by an on-hybrid thermistor expressed in deg. C (resolution non linear, best = 0.083 deg. C)
    */
    float GetHybridTemperature (uint8_t pSlaveChipSelectId = ADC_SLAVE_ID);
    float ConvertADCtoTemperature (float pADCValue);

    usb_dev_handle* find_antenna_usb_handle();

    usb_dev_handle* setup_libusb_access();

  private:
    usb_dev_handle* fUsbHandle;
    const static int fUsbEndpointBulkIn = 0x82;  // usb endpoint 0x82 address for USB IN bulk transfers
    const static int fUsbEndpointBulkOut = 0x01;  // usb endpoint 0x01 address for USB OUT bulk transfers
    const static int fUsbTimeout = 5000;  // usb operation timeout in ms

    //setting
    const static int fAdcSlaveCS = 4;
    //const static int fAdcSlaveCS = 4;
    //const static int fAdcSlaveCS = 4;


};
#endif
